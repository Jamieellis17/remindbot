const PORT = process.env.PORT;
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const API_BASE_URL = 'https://api.atlassian.com';

if (!PORT || !CLIENT_ID || !CLIENT_SECRET) {
  console.log("Usage:");
  console.log("PORT=<http port> CLIENT_ID=<app client ID> CLIENT_SECRET=<app client secret> node app.js");
  process.exit();
}

const _ = require('lodash');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const request = require('request');
const app = express();
const cors = require('cors');
const moment = require ('moment');
const jsonpath = require('jsonpath');
const Document = require('adf-builder').Document;
const prettyjson = require('prettyjson');
const now = moment()
const thetime = now.format('HH:mm Z')
const schedule = require('node-schedule');


var mysql = require('mysql');
const messageReader = require('./messagereader');
const stride = require('./stride').factory({
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
});
app.use(bodyParser.json());
app.use(express.static('.'));

var connection = mysql.createConnection({
  host     : 'localhost',
  port     : '8889',
  user     : 'root',
  password : 'root',
  database : 'remindbot'
});

function timeconvert(time) {

  var hours = parseInt(time.substr(0, 2));
  if(time.indexOf('am') != -1 && hours == 12) {
      time = time.replace('12', '0');
  }
  if(time.indexOf('pm')  != -1 && hours < 12) {
      time = time.replace(hours, (hours + 12));
  }
  return time.replace(/(am|pm)/, '');
}



function getAccessToken(callback) {
  const options = {
    uri: 'https://auth.atlassian.com/oauth/token',
    method: 'POST',
    json: {
      grant_type: "client_credentials",
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      "audience": "api.atlassian.com"
    }
  };
  request(options, function (err, response, body) {
    if (response.statusCode === 200 && body.access_token) {
      callback(null, body.access_token);
    } else {
      callback("oh noe! could not generate access token: " + JSON.stringify(response));
    }
  });
}

function sendMessage(cloudId, conversationId, messageTxt, callback) {
  getAccessToken(function (err, accessToken) {
    if (err) {
      callback(err);
    } else {
      const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
      const options = {
        uri: uri,
        method: 'POST',
        headers: {
          authorization: "Bearer " + accessToken,
          "cache-control": "no-cache"
        },
        json: {
          body: {
            version: 1,
            type: "doc",
            content: [
              {
                type: "paragraph",
                content: [
                  {
                    type: "text",
                    text: messageTxt
                  }
                ]
              }
            ]
          }
        }
      }

      request(options, function (err, response, body) {
        callback(err, body);
      });
    }
  });
}

async function checkReminders() { 
  
     connection.query("SELECT * FROM `reminders` ", function(error, rows, fields) {
      for (var i in rows) {
        remindtime = rows[i].time;
        var date9 = new Date();
        const cloudId = rows[i].cloudid;
        const userId = rows[i].username;
        const conversationId = rows[i].conversationid;
        const event = rows[i].reminder;
        const eventid = rows[i].id;
        const alertText = "Hey there, {{MENTION}} It's time to go to your "+ event +". Good luck! ";

        var current_time = date9.getHours() + ':' + date9.getMinutes();
        

        console.log('debug: alertid: '+ eventid+ ' current hour: ' + current_time + ", remind time: " + remindtime);
        if (current_time >= remindtime) {
          connection.query("DELETE FROM `reminders` WHERE `reminders`.`id` = "+ eventid)          
           return stride.createDocMentioningUser({ cloudId, userId, text: alertText }).then((document) => {
            return stride.sendMessage({cloudId, conversationId, document})
           });
        }
    }
   });
 }

  //stride.createDocMentioningUser({ cloudId, userId, text: alertText })
   
  async function doit() {
    await checkReminders();
    setTimeout(doit, 5*1000);
  }
  setTimeout(doit, 5*1000);
  
function sendReply(message, replyTxt, callback) {
  const cloudId = message.cloudId;
  const conversationId = message.conversation.id;
  const userId = message.sender.id;

  sendMessage(cloudId, conversationId, replyTxt, function (err, response) {
    if (err) {
      console.log('Error sending message: ' + err);
      callback(err);
    } else {
      callback(null, response);
    }
  });
}


app.post('/installed',
  function (req, res) {
    console.log('app installed in a conversation');
    const cloudId = req.body.cloudId;
    const conversationId = req.body.resourceId;
    sendMessage(cloudId, conversationId, "What's up doc? Thanks for adding me here.. To use me, use the following syntax: @me remind me to go home at 5pm", function (err, response) {
      if (err)
        console.log(err);
    });
    res.sendStatus(204);
  }
);

/**
 * @bot schedule a meeting for 9am
 * 
 * 
 */ 

/**
** the main attraction. 
*/


app.post('/bot-mention',
  async function (req, res) {
    let user;
    console.log('detected mention, checking for correct syntax. ');
    const reqBody = req.body;
    const messageText = reqBody.message.text;
    const messagethingtoSchedule = reqBody.message.text.split(" ").slice(1).join(" ");
    const cloudId = reqBody.cloudId;
    
    const event2 = reqBody.message.text.split(" a ").slice(1).join(" ");
    const event = event2.replace(/ .*/,'');
    const time = reqBody.message.text.split(" ").slice(5).join(" ");
    const messagetimetoSchedule = reqBody.message.text;

    if(messageText.indexOf("schedule", ":")>=0){
      console.log ("we've got one guys, i'll take it from here. ")
      console.log (timeconvert(time));
      
      const time24 = timeconvert(time);
      
      // user = await stride.getUser({
      //   cloudId,
      //   userId: reqBody.sender.id,
      // });
      
          const doc = new Document();
      
          const card = doc.applicationCard('Reminder set! ')
            .link('https://www.atlassian.com')
            .description('I\'m scheduled to remind you at ' + time + ' for your ' + event + '')
            .background('https://' + reqBody.host + '/img/card-background.jpg');
          card.action()
            .title("Cancel Reminder")
            .target({key: "refapp-action-cancelEvent"})
            .parameters({then: "done"});

          card.context("RemindBot v0.2")
            .icon({url: "https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png", label:"Stride"});
      
          const document = doc.toJSON();
      
           stride.reply({reqBody, document});
           

           connection.query("INSERT INTO `reminders` (`id`, `cloudid`, `conversationid`, `username`, `reminder`, `time`, `fullmsg`) VALUES (NULL, '"+ reqBody.cloudId + "', '"+ reqBody.conversation.id + "', '"+ reqBody.sender.id +"', '"+ event +"', '"+ time24 +"', '"+ messageText +"');")
           
    } else {
      console.log ("nope, i'm not needed here. oh well, they probably needed a friend to talk to. ")
      sendReply(req.body, "Well, this is awkward. Please use the correct syntax. (@RemindMe schedule a *meeting* for *9:00am*) and if you only put 9am, please do 9:00am.. i'm not that advanced yet.", function (err, response) {
        if (err) {
          console.log(err);
        } 
      });
    } /** end syntax check btw */
      
    
    console.log("by the way, here's the full message.. " + messageText);
  
    res.sendStatus(204);
    
    // sendReply(req.body, "Give me a second.", function (err, response) {
    //   if (err) {
    //     console.log(err);
    //     res.sendStatus(500);
    //   } else {
        
    //   }
    // });
  }
);

/**
 * no need to worry bout this my dude. 
 */
app.get('/descriptor', function (req, res) {
  fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
    const template = _.template(descriptorTemplate);
    const descriptor = template({
      host: 'https://' + req.headers.host
    });
    res.set('Content-Type', 'application/json');
    res.send(descriptor);
  });
});

http.createServer(app).listen(PORT, function () {
  console.log('Hey there, i\'m running on port ' + PORT);
});


app.options('/module/action/refapp-service', cors());
app.post('/module/action/refapp-service',
cors(),
stride.validateJWT,
(req, res) => {
  console.log('Received a call from an action in a message' + prettify_json(req.body));
  const cloudId = res.locals.context.cloudId;
  const conversationId = res.locals.context.conversationId;
  const parameters = req.body.parameters;
  var response = {
    message: "Done!"
  };

  res.send(JSON.stringify(response));
  stride.sendTextMessage({cloudId, conversationId,
    text: "A button was clicked! The following parameters were passed: " + JSON.stringify(parameters)})
    .then(() => res.send(JSON.stringify({})));
}
);
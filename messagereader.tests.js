const { expect } = require('chai');
const { describe, it } = require('mocha');

const messageReader = require('./messagereader');

describe('message reader', () => {
    it('returns blah when given plaaahgh', () => {
        const input = "@remind schedule a meeting for 3pm";

        const result = messageReader(input);
        
        expect(result).is.deep.eq({
            event: 'meeting',
            time: '3pm',
        });
    });
})
